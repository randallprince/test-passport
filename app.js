// includes
var express 	= require( 'express' );
var https 		= require( 'https' );
var http 		= require( 'http' );
var fs 			= require( 'fs' );

// this line is from the Node.js HTTPS documentation.
var options = {

	domain      : "localhost",
	key 		: fs.readFileSync( 'ssl/privatekey.pem'  ),
	cert 		: fs.readFileSync( 'ssl/certificate.pem' ),
	porthttp 	: 8888,
	porthttps	: 8889
};

// create a service (the app object is just a callback).
var app = express();

// http and https servers
var http  = http.createServer( app ).listen( options.porthttp );
var https = https.createServer( options, app ).listen( options.porthttps );

app.get( '/', function( req, res ) {

	if ( req.secure ) {

		res.send( 'Hello [Secure] World!' );

	} else {

		res.send( 'Hello World!' );

		var forward = 'https://' + options.domain + ':' + options.porthttps + '/';
		console.log( forward );
		// res.redirect( 301, forward );
	}
});